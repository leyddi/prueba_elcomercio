<?php
class ClearPar{
    public function build($cadena){
        $salida="";
        for($i=0;$i<strlen($cadena);$i++){
            $aux = '()';
            $pos = strpos($cadena, $aux);
            $cadena = substr_replace($cadena, '', $pos, 2);
            $salida = $salida. "()";
            if(is_bool($pos)){
                break;
            }
        }
        return $salida;
    }
}
/*
$funcion = new ClearPar;
print_r( $funcion->build("(()()()()(()))))())((())"));
*/

?>