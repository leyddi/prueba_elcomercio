<?php

class Busqueda{
        public static function array_l(){
            $data = file_get_contents("./../src/employees.json");
            $array = json_decode($data);

            return $array;
        }
        public static function search(){
            $array = Busqueda::array_l();
           
            if(isset($_GET["email"]) && $_GET["email"]!=""){
                foreach($array as $item)
                {   $email  = $item->email;
                    $email=str_replace("%40","@",$email);
                    if($email ==$_GET["email"])
                    {   
                        return  array($item);                
                    }
                }
                return array();
            }else{
                return $array;
            }
        }
        public static function detail($id){
            $array = Busqueda::array_l();

            foreach($array as $item)
                {   if($item->id ==$id) {   
                        return  array($item);                
                    }
                }
        }
        public static function salaryRange($min,$max){
            $array = Busqueda::array_l();
            $salida = array();
            $min_a = floatval(preg_replace('/[^\d\.]/', '', $min));
            $max_a = floatval(preg_replace('/[^\d\.]/', '', $max));
            foreach($array as $item)
            {   
                $salary =  floatval(preg_replace('/[^\d\.]/', '',$item->salary)); 
                if($salary>= $min_a && $salary<=$max_a) {   
                    array_push($salida,$item); 
                }
            }
            return $salida;
        }
       
}        
?>
