<?php

use Slim\Http\Request;
use Slim\Http\Response;
require_once("./../src/services/search.php");
// Routes



$app->get('/[{name}]', function (Request $request, Response $response, array $args) {
    $loader = new Twig_Loader_Filesystem('./../templates');
    $twig = new Twig_Environment($loader);
    $aux =new Busqueda();
    if(isset($_GET["salarymin"]) && isset($_GET["salarymax"])){
        return $twig->render('salary.xml.twig',['employees'=> $aux->salaryRange($_GET["salarymin"],$_GET["salarymax"])]);
        
    }
        return $twig->render('index.phtml',['employees'=> $aux->search()]);
    
    
});
$app->get('/detail/[{id}]', function (Request $request, Response $response, array $args) {
  
    $loader = new Twig_Loader_Filesystem('./../templates');
    $twig = new Twig_Environment($loader);
    $aux =new Busqueda();
    
    return $twig->render('detail.html',['employee'=> $aux->detail($args['id'])]);

});
 