<?php

class CompleteRange{
    
	public function build($array){
	
        $salida=array();
	
        for($i=min($array); $i<max($array)+1;$i++){
	
            array_push($salida,$i);
        
		}

        return $salida;
    
	}

}

/*
$funcion = new CompleteRange;

$array = array(2,4,9);

print_r( $funcion->build($array));
*/
?>